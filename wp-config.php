<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'pa-idf2022' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', 'root' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'qe2c8LA zA56EasUgJ+{t!m3-[(EBIZ+1Iej@5!w023)t?I!@TkMOTCx}`f<cY($' );
define( 'SECURE_AUTH_KEY',  '>NXyr} fX3L)42v^}fD=zmSGzxkODVNa+c*lZvy`k,cjQ<RtsWUYyBhn?B|i6P=F' );
define( 'LOGGED_IN_KEY',    'S; hLDF8<`cxdR:M-OF#u)(2WhYC[P&ygX:8_Phs?w_sk1].u0l*&xCAwE1-TOcv' );
define( 'NONCE_KEY',        ')rs*#S)LDf!z*$,fxQBRS?_v)<B1.37lvWZsNb`,O$::!F%&?Ru Gq]B)=T,Jg.W' );
define( 'AUTH_SALT',        'X9sGNxDkmbNK@P W{%D8ebzO~9!;Vd_>XJyV<R]w5>;,>|i~o)rvoRMlU7YHmZE7' );
define( 'SECURE_AUTH_SALT', '_o$NgM-T118l#dwRx^JhTn8?v^:1O[~`=0L->0IH[84)R5N[yB;1 Q:#*:)yt5fE' );
define( 'LOGGED_IN_SALT',   'cyM4u^L4^{kwe.QGRefWk J,XI*U#Mn[SP<_;3#]H^tss[:89jv8#wA/MANlKB 9' );
define( 'NONCE_SALT',       'it>/T7Lu0oE@{zTfGjz>9<?mH56M/:A}M}k|Pkvyj&EU)rXM``pesa-Q&U:L|$!R' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
